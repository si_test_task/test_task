## Requirements
Project is configured to use `JDK version 17` 

## Test execution
Test execution can be triggered either by running the `TestRunner` class or by running one of the following commands from the command line in the project's root directory:

1. Using the maven wrapper that is included with the project, without worrying about installing a specific version of maven
```bash
./mvnw clean verify
```
2. Using already installed  maven with version 3.6.3 or higher

```bash
mvn clean verify
```
After the test run execution results will be displayed in the console window, or they can be reviewed in [html report](target/site/serenity/index.html), which will be generated after each run   
In case tests are running in GitLab CI/CD pipeline test results should be published to [GitLab Pages](https://si_test_task.gitlab.io/test_task/) of this repository

## Implementing new tests

New tests could be implemented by adding new .feature files with test scenarios to the project's `resource` folder

In case new steps were used to write the scenarios it is required to implement them by creating corresponding Step Definition classes in `src/test/java/starter/stepdefinitions` package
These Step Definition classes should contain an implementation of all newly created/used steps
Names of that classes should be chosen corresponding to the area they cover

For an example please refer to `src/test/java/starter/stepdefinitions/SearchStepDefinitions.java` and `src/test/resources/features/search/post_product.feature`

## Changes to the repository

1. Removed support for gradle build scripts
   - The requirements of the task was to use Maven as the build  tool and there was no reason to fix the broken dependencies of build.gradle file
2. Added maven wrapper
   - Maven wrapper adds the ability to build and execute tests without installation of specific version of maven that required by project
3. Refactor & Cleanup
   1. Removed CarsAPI class because it was unused and not relevant to the tested API
   2. Removed dependencies and properties for webdriver and any of UI relevant libraries since in that specific case we only were using RestAssured for testing 
   3. Removed maven surefire plugin since we are not executing any unit tests
   4. Removed properties and configurations for artifactory since we are not deploying any artifacts at the moment. All of it can be added on-demand later
   5. Removed assertj dependencies since all the assertions is performed using rest-assured
   6. Since we are not uploading any artifact - added maven-jar-plugin and configured it to not generate empty jar files
   7. Completely rewrote the SearchStepDefinition steps to make steps test at-least something :) 
   8. Moved baseurl and searchpath to `serenity.conf` that adds the ability to execute tests against different environments (Assuming the test data is the same)
4. Added configuration for GitLab CI/CD pipeline (`.gitlab-ci.yml`)
5. Added a schedule for pipeline to execute tests on a daily basis
6. Configured `.gitlab-ci.yml` to publish the serenity html report to the GitLab Pages of the project in case test were executed in master branch
   - Pushing in different branches also triggers tests but result will not be published to GitLab pages. Instead, they could be reviewed as the artifacts of the `test` stage of the pipeline.    
    To review them simply go to the CI/CD Pipelines tab by choosing it in the menu of the repository or by accessing it via direct link https://gitlab.com/si_test_task/test_task/-/pipelines (Assuming you have access to this page)
    Locate the pipeline with your changes and select the first (test) stage of the pipeline
  
   ![img.png](assets/pipeline_stage_selection.png)
   
   After that you should see an option to `Browse` the Job artifacts on the right side of the window 

   ![img.png](assets/browse_artifacts.png)
   
   From there you can simply navigate through the artifact to the serenity folder and open the `index.html` file that usually located almost at the end of the list
   
   ![img.png](assets/serenity_folder_path.png)

  ![img.png](assets/index_html.png)
   

   