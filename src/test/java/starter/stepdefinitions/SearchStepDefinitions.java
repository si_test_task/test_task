package starter.stepdefinitions;

import io.cucumber.java.Before;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.serenitybdd.core.environment.EnvironmentSpecificConfiguration;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.util.EnvironmentVariables;

import static org.hamcrest.Matchers.*;

public class SearchStepDefinitions {

    EnvironmentVariables environmentVariables;
    String baseSearchUrl;
    @Before
    public void beforeFeatures() {
        String baseUrl =  EnvironmentSpecificConfiguration.from(environmentVariables)
                .getProperty("restapi.baseurl");
        String searchPath =  EnvironmentSpecificConfiguration.from(environmentVariables)
                .getProperty("restapi.searchpath");
        baseSearchUrl = baseUrl.concat(searchPath);
    }

    @When("user search for product {string}")
    public void userSearchForProduct(String product) {
        String url = baseSearchUrl.concat(product);
        SerenityRest.given().get(url);
    }

    @Then("user sees that there is no errors displayed")
    public void userSeesNoErrors() {
        SerenityRest.restAssuredThat(response -> response.statusCode(200));
        SerenityRest.restAssuredThat(response -> response.body("detail.error", empty()));
    }

    @Then("user sees that some results are displayed")
    public void userSeesResultsDisplayedForProduct() {
        SerenityRest.restAssuredThat(response -> response.body("$", hasItems(hasKey("title"))));
    }

    @Then("user sees that any results are NOT displayed")
    public void userSeesResultsNotDisplayedForProduct() {
        SerenityRest.restAssuredThat(response -> response.body("$", not(hasItems(hasKey("title")))));
    }

    @Then("user sees that there is an error displayed")
    public void userSeesThatThereIsAnErrorDisplayed() {
        SerenityRest.restAssuredThat(response -> response.body("detail.error", is(true)));
    }
}
