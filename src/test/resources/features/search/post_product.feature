Feature: Search for the product

### Please use endpoint GET https://waarkoop-server.herokuapp.com/api/v1/search/demo/{product} for getting the products.
### Please use endpoint GET https://waarkoop-server.herokuapp.com/api/v1/search/demo/orange for getting the products.
### Available products: "orange", "apple", "pasta", "cola"
### Prepare Positive and negative scenarios

  Scenario Outline: Search for existing products
    When user search for product '<product_name>'
    Then user sees that there is no errors displayed
    Then user sees that some results are displayed

    Examples:
      | product_name |
      | orange       |
      | apple        |
      | pasta        |
      | cola         |

  Scenario Outline: Search for not existing products
    When user search for product '<product_name>'
    Then user sees that there is an error displayed
    Then user sees that any results are NOT displayed

    Examples:
      | product_name |
      | car          |
      | plane        |
